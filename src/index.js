/**
 * App ID for the skill
 */
var APP_ID = undefined; //replace with 'amzn1.echo-sdk-ams.app.[your-unique-value-here]';

/**
 * The AlexaSkill Module that has the AlexaSkill prototype and helper functions
 */
var AlexaSkill = require('./AlexaSkill');

/**
 * RollDieSkill is a child of AlexaSkill.
 * To read more about inheritance in JavaScript, see the link below.
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript#Inheritance
 */
var RollDieSkill = function() {
    AlexaSkill.call(this, APP_ID);
};

// Extend AlexaSkill
RollDieSkill.prototype = Object.create(AlexaSkill.prototype);
RollDieSkill.prototype.constructor = RollDieSkill;

RollDieSkill.prototype.eventHandlers.onSessionStarted = function (sessionStartedRequest, session) {
    console.log("RollDieSkill onSessionStarted requestId: " + sessionStartedRequest.requestId
        + ", sessionId: " + session.sessionId);

    // any session init logic would go here
};

RollDieSkill.prototype.eventHandlers.onLaunch = function (launchRequest, session, response) {
    console.log("RollDieSkill onLaunch requestId: " + launchRequest.requestId + ", sessionId: " + session.sessionId);
    getWelcomeResponse(response);
};

RollDieSkill.prototype.eventHandlers.onSessionEnded = function (sessionEndedRequest, session) {
    console.log("onSessionEnded requestId: " + sessionEndedRequest.requestId
        + ", sessionId: " + session.sessionId);

    // any session cleanup logic would go here
};

RollDieSkill.prototype.intentHandlers = {

    "GetDieIntent": function (intent, session, response) {
        handleDieIntentRequest(intent, session, response);
    },
    "GetVersionIntent": function (intent, session, response) {
        handleVersionIntentRequest(intent, session, response);
    },
    "AMAZON.HelpIntent": function (intent, session, response) {
        var speechText = "Die Roller will roll a die of any number of sides" +
            "For example, you can ask 'roll one dee twelve' and get a random number between 1 and 12"
        var repromptText = "Roll a die?";
        var speechOutput = {
            speech: speechText,
            type: AlexaSkill.speechOutputType.PLAIN_TEXT
        };
        var repromptOutput = {
            speech: repromptText,
            type: AlexaSkill.speechOutputType.PLAIN_TEXT
        };
        response.ask(speechOutput, repromptOutput);
    },
    // {
    // "AMAZON.WelcomeIntent": function(intent, session, response) {
    //     response.ask("Hello, I'm here to roll your die. What can I roll for you?", "How many sides does this die have?");
    // },

    "AMAZON.StopIntent": function (intent, session, response) {
        var speechOutput = {
                speech: "Goodbye",
                type: AlexaSkill.speechOutputType.PLAIN_TEXT
        };
        response.tell(speechOutput);
    },

    "AMAZON.CancelIntent": function (intent, session, response) {
        var speechOutput = {
                speech: "Goodbye",
                type: AlexaSkill.speechOutputType.PLAIN_TEXT
        };
        response.tell(speechOutput);
    }
};

/**
 * Function to handle the onLaunch skill behavior
 */

function getWelcomeResponse(response) {
    // If we wanted to initialize the session to have some attributes we could add those here.
    var cardTitle = "Die Roller";
    var repromptText = "Would you like me to roll a die?";
    var speechText = "How many sides does this die have?";
    // If the user either does not reply to the welcome message or says something that is not
    // understood, they will be prompted again with this text.

    var speechOutput = {
        speech: "<speak>" + speechText + "</speak>",
        type: AlexaSkill.speechOutputType.SSML
    };
    response.ask(speechOutput, repromptText);
}

/**
 * Gets a poster prepares the speech to reply to the user.
 */
function handleVersionIntentRequest(intent, session, response) {

    var cardTitle = "DieRoller Version";
    var cardContent = "0.1.3";

    var speechOutput = {
        speech: "<speak>I am version zero dot zero dot three</speak>",
        type: AlexaSkill.speechOutputType.SSML
    };

    response.tellWithCard(speechOutput, cardTitle, cardContent);
}

/**
 * Gets a poster prepares the speech to reply to the user.
 */
function handleDieIntentRequest(intent, session, response) {

    var result = "Your result is ";

    if ( intent.slots.Sides ) {
		result +=  dieRoll(intent.slots.Sides.value);
    }
    else {
	    // uh inform the user that we don't know how many sides.
	    result += " impossible to determine since I did not hear a number of sides.";
    }

        var speechOutput = {
            speech: "<speak>"+ result + "</speak>",
            type: AlexaSkill.speechOutputType.SSML
        };
        response.tell(speechOutput);
}

// this is in its own function to facilitate calling it multiple times.
function dieRoll(sides) {
	return Math.floor(Math.random() * (sides) + 1);
}

// Create the handler that responds to the Alexa Request.
exports.handler = function (event, context) {
    // Create an instance of the HistoryBuff Skill.
    var skill = new RollDieSkill();
    skill.execute(event, context);
};

