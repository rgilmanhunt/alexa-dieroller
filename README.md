# Die Roller AWS Lambda

## Concepts

This die roller will ... roll a die. Designed primarily for tabletop game arbitration,
ask the die roller for a "d 20" (pronounced dee 20). You'll have to do extra math on your side
if you need something like "d8+1" or "3d6". 

## Example user interactions:
"Alexa ask die roller to roll me a d 12"

## Future Plans
I plan to have it take multiple dice as well (roll three dee six)

## Setting up an alexa skill is outside the scope of this document.
